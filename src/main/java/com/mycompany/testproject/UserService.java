/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.testproject;

import java.util.ArrayList;

/**
 *
 * @author sairung
 */
public class UserService {

    private static ArrayList<User> userList = new ArrayList<>();

    static {
        userList.add(new User("admin", "password"));
        
    }
    //Add user

    public static boolean addUser(User user) {
        userList.add(user);
        return true;
    }

    public static boolean addUser(String username, String password) {
        userList.add(new User(username, password));
        return true;
    }
//Update user

    public static boolean updateUser(int index, User user) {
        userList.set(index, user);
        return true;
    }
//Read user

    public static User getUser(int index) {
        if(index>userList.size()-1){
            return null;
        }
        return userList.get(index);
    }

    public static ArrayList<User> getUsers() {
        return userList;
    }
    // Delate user

    public static boolean delUser(int index) {
        userList.remove(index);
        return true;
    }

    public static boolean delUser(User user) {
        userList.remove(user);
        return true;
    }

    //Login
    public static User login(String userName, String password) {
        for (User user : userList) {
            if (user.getUsername().equals(userName) && user.getPassword().equals(password)) {
                return user;
            }
        }
        return null;
    }
public static void save(){
    
}
public static void load(){
    
}

}
